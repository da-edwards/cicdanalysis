using CICDAnalysis.Website.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace CICDAnalysis.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            HomeController homeController = new HomeController();

            var result = homeController.Index() as ViewResult;

            Assert.Equal(
                "1.0.0.5",
                result.ViewData["Message"].ToString());
        }
    }
}
